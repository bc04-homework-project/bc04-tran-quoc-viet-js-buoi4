function guest() {
  var canh1 = document.getElementById("canh1").value * 1;
  var canh2 = document.getElementById("canh2").value * 1;
  var canh3 = document.getElementById("canh3").value * 1;

  var result;

  if (
    canh1 * canh1 - canh2 * canh2 - canh3 * canh3 == 0 ||
    canh2 * canh2 - canh3 * canh3 - canh1 * canh1 == 0 ||
    canh3 * canh3 - canh1 * canh1 - canh2 * canh2 == 0
  ) {
    result = "Tam giác vuông";
  } else if (canh1 == canh2 && canh1 == canh3) {
    result = "Tam giác đều";
  } else if (canh1 == canh2 || canh1 == canh3 || canh2 == canh3) {
    result = "Tam giác cân";
  } else {
    result = "Tam giác khác";
  }

  // console.log(result)

  document.getElementById("result").innerHTML = result;
}
